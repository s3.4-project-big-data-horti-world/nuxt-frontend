export const state = () => ({
    images: {},
    image: {},
    uploadedImages: 0,
    processedImages: 0,
    healthyImages: [],
    healthyPercentage: 0,
    unhealthyImages: [],
    unhealthyPercentage: 0,
});

export const actions = {
    // Get processed images by specific date range
    async getByDateRange({ commit }, dateRange) {
        
        try {
            const response = await this.$axios.get(backendUrl(`images/from/${dateRange.from}/until/${dateRange.till}`));
            
            if (200 <= response.status && response.status < 300) {
                commit("saveImages", response.data);
            };
        } catch (error) {
            console.log(error);
        }
    },

    // Upload images to process
    async uploadImages({}, formData) {
        
        let config = {
            headers: {
              'content-type': 'multipart/form-data'
            }
        }

        try {
            const response = await this.$axios.post(backendUrl('images'), formData, config);
            if (200 <= response.status && response.status < 300) {
                commit("saveImages", response.data);
            };
        } catch (error) {
            console.log(error);
        }
    }
}

export const mutations = {
    saveImages(state, images) {
        state.images = images;
        state.uploadedImages = Object.keys(images).length;
        state.processedImages = Object.keys(images).length;

        state.healthyImages = [];
        state.unhealthyImages = [];
        Object.keys(images).forEach((key) => {
            if(images[key].healthy === 1) {
                state.healthyImages.push(images[key]);
            } else {
                state.unhealthyImages.push(images[key]);
            }
        });
        
        const healthyPercentage = (state.healthyImages.length / state.uploadedImages * 100).toFixed(1);
        const unhealthyPercentage = (state.unhealthyImages.length / state.uploadedImages * 100).toFixed(1)

        state.healthyPercentage = healthyPercentage === 'NaN' ? '0' : healthyPercentage;
        state.unhealthyPercentage = unhealthyPercentage === 'NaN' ? '0' : unhealthyPercentage;
    },
}

// Function that gets the env backend url and add suffix
const backendUrl = (suffix) => {
    return `${process.env.backendUrl}/${suffix}`;
}
