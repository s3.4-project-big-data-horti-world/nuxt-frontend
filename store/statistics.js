export const state = () => ({
    uploaded: 0,
    processed: 0,
    percentageHealthy: '0',
    percentageUnhealthy: '0',
    modelAccuracy: 0,
    modelValidationLoss: 0
});

export const actions = {
    async getStats({ commit }) {
        try {
            const response = await this.$axios.get(backendUrl("stats/"));

            if (200 <= response.status && response.status < 300) {
                commit("saveStats", response.data);
            };
        } catch (error) {
            console.log(error);
        }
    },
}

export const mutations = {
    saveStats(state, stats) {
        state.uploaded = stats.uploaded_images;
        state.processed = stats.processed_images;
        state.percentageHealthy = stats.percentage_healthy_tomatoes;
        state.percentageUnhealthy = stats.percentage_unhealthy_tomatoes;
        state.modelAccuracy = stats.model_accuracy;
        state.modelValidationLoss = stats.model_validation_loss;
    }
}

// Function that gets the env backend url and add suffix
const backendUrl = (suffix) => {
    return `${process.env.backendUrl}/${suffix}`;
}
