import Vue from 'vue'
import Vcalendar from 'v-calendar'

Vue.use(Vcalendar, {
    locales: {
        'farmer': {
            firstDayOfWeek: 1,
            masks: {
                L: 'DD-MM-YYYY',
                // ...optional `title`, `weekdays`, `navMonths`, etc
            }
        }
    }
})