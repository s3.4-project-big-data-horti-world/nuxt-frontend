FROM node:12 AS base

WORKDIR /srv

COPY package*.json /srv/

RUN npm install

COPY . /srv/

EXPOSE 80

# production build
FROM base AS prod

CMD npm run build && npm run start

# development build
FROM base AS dev

CMD npm run dev
